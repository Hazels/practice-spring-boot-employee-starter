package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    EmployeeService employeeService;
    public List<Company> companyList = new ArrayList<>(Arrays.asList(
            new Company(1L, "aaa"),
            new Company(2L, "bbb"),
            new Company(3L, "CCC"),
            new Company(4L, "DDD")));

    public List<Company> getAllCompanies() {
        return companyList;
    }

    public Company getCompany(Long Cid) {
        return companyList.stream().filter(
                company -> company.getId().equals(Cid)
        ).findFirst().orElse(null);
    }

    public List<Employee> getEmployeesUnderCompany(Long cid) {
        return employeeService.getAllEmployees().stream().
                filter(employee -> employee.getCompanyId().equals(cid)).collect(Collectors.toList());
    }

    public List<Company> pageQueryEmployees(Integer page, Integer size) {
        return companyList.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());

    }

    public void createCompany(Company company) {
        company.setId(nextId());
        companyList.add(company);
    }

    public Long nextId() {
        return companyList.stream()
                .map(company -> company.getId())
                .max((o1, o2) -> (int) (o1 - o2))
                .orElse(0L) + 1;
    }

    public void updateCompany(Company company) {
        Company company1 = companyList.stream().filter(company2 -> company2.getId().equals(company2.getId()))
                .findFirst()
                .orElse(null);
        if (company1 != null) {
            company1.setName(company.getName() == null ? company1.getName() : company.getName());
        }
    }

    public void deletCompany(Long cid) {
        companyList = companyList.stream().filter(company -> !company.getId().equals(cid)).collect(Collectors.toList());
    }
}
