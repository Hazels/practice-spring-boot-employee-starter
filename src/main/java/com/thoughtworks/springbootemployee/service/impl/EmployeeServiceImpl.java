package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    public List<Employee> employeeList = new ArrayList<>(Arrays.asList(
            new Employee(1L, "aaa", 20, "female", 2.0, 1L),
            new Employee(2L, "aaa", 20, "female", 2.0, 2L),
            new Employee(3L, "aaa", 20, "female", 2.0, 3L),
            new Employee(4L, "aaa", 20, "male", 2.0, 4L)));

    public List<Employee> getAllEmployees() {
        return employeeList;
    }

    public Employee getEmployee(Long id) {
        return employeeList.stream().filter(
                employee -> employee.getId().equals(id)
        ).findFirst().orElse(null);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employeeList.stream().filter(
                employee -> employee.getGender().equals(gender)
        ).collect(Collectors.toList());
    }

    public void createEmployee(Employee employee) {
        employee.setId(nextId());
        employeeList.add(employee);
    }

    public Long nextId() {
        return employeeList.stream()
                .map(employee -> employee.getId())
                .max((o1, o2) -> (int) (o1 - o2))
                .orElse(0L) + 1;
    }

    public void updateEmployee(Employee employee) {
        Employee employee2 = employeeList.stream().
                filter(employee1 -> employee1.getId().equals(employee.getId()))
                .findFirst()
                .orElse(null);
        if (employee2 != null) {
            employee2.setName(employee.getName() == null ? employee2.getName() : employee.getName());
            employee2.setAge(employee.getAge() == null ? employee2.getAge() : employee.getAge());
            employee2.setGender(employee.getGender() == null ? employee2.getGender() : employee.getGender());
            employee2.setSalary(employee.getGender() == null ? employee2.getSalary() : employee.getSalary());
        }
    }

    public void deleteEmployee(Long id) {
        employeeList = employeeList.stream().filter(employee -> !employee.getId().equals(id)).collect(Collectors.toList());
    }

    public List<Employee> pageQueryEmployees(Integer page, Integer size) {
        return employeeList.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }
}
