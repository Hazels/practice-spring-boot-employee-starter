package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;

public interface CompanyService {

    public List<Company> getAllCompanies();

    public Company getCompany(Long cid);

    public List<Employee> getEmployeesUnderCompany(Long cid);

    List<Company> pageQueryEmployees(Integer page, Integer size);

    public void createCompany(Company company);

    public void updateCompany(Company company);

    public void deletCompany(Long cid);
}
