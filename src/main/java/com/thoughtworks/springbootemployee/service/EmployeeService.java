package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;

public interface EmployeeService {
    public Employee getEmployee(Long id);

    public List<Employee> getAllEmployees();

    public List<Employee> getEmployeeByGender(String gender);

    public void createEmployee(Employee employee);

    public Long nextId();

    public void updateEmployee(Employee employee);

    public void deleteEmployee(Long id);

    public List<Employee> pageQueryEmployees(Integer page, Integer size);
}
