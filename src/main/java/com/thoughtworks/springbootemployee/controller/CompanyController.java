package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("companies")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @GetMapping()
    public List<Company> getAllCompanies() {
        return companyService.getAllCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompany(@PathVariable("id") Long cid) {
        return companyService.getCompany(cid);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesUnderCompany(@PathVariable("id") Long cid) {
        return companyService.getEmployeesUnderCompany(cid);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> pageQueryCompanies(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return companyService.pageQueryEmployees(page, size);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void createCompany(@RequestBody Company company) {
        companyService.createCompany(company);
    }

    @PutMapping()
    public void updateCompany(@RequestBody Company company) {
        companyService.updateCompany(company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable("id") Long cid) {
        companyService.deletCompany(cid);
    }
}
