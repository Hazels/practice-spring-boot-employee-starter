# Daily Report (2023/07/18)
# O (Objective): 
## 1. What did we learn today?
### We learned HTTP Basic & RESTful, Pair programming and SpringBoot today.

## 2. What activities did you do? 
### I conducted a code review and showed the result of brain storm with my team members, and had pair programming to practice SpringBoot with Jaden.

## 3. What scenes have impressed you?
### I have been impressed by drawing the concept map of TDD with my team members today.
---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Sad.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is showing the result of the concept map of TDD with the team members. Because this not only let me have a deeper understanding of TDD and Agile development, but also let me exercise my organizational and expressive skills.


# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use Java stream instead of loop and try to reduce the use of if else statements as much as possible when I write the functions, and I will pair programming during development.
### I will develop a good coding habit of committing step by step. And I will strengthen communication with group members when doing group assignments in order to set aside time to rehearse the content of the speech in advance and present better performance results.



